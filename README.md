# CV web

## Description

CV de Loïne Adamczak fait en HTML et CSS dans le cadre d'un TP de conception WEB en 3<sup>ème</sup> année de Licence Audiovisuel

## Visual

![visual1](visuals/visual1.png )
![visual2](visuals/visual2.png)
![visual3](visuals/visual3.png)
![visual4](visuals/visual4.png)


## Outils

Utilisation d'une grille CSS afin de facilité le placement des éléments dans la page (simple-grid.css)
